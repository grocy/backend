package me.rasztabiga.grocy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GrocyApplication

fun main(args: Array<String>) {
	runApplication<GrocyApplication>(*args)
}
